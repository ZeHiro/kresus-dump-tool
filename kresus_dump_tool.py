#!/usr/bin/python
# -*- coding: utf8 -*-
"""
This module fixes the consistancy of a banks.json file exported from Kresus.
"""
from builtins import str, range

import json
import os
import click


@click.pass_context
def error(ctx, message):
    """
    Prints an error message and end the program.
    :param: ctx: Context passed by @click.pass_context.
    :params: message: Error message to print.
    """
    click.secho(message, fg='red')
    ctx.exit(1)


def format_account(account):
    """
    Formats an account to display to the user.
    :param: account: An accout.
    """
    return '%s (number: %s)' % (account['title'], account['accountNumber'])


def load_json_file(ctx, param, file_path):
    try:
        with open(file_path, 'r') as file_descriptor:
            content = json.load(file_descriptor)
    except IOError:
        error('Could not open file at path %s' % click.format_filename(os.path.abspath(file_path)))
    except ValueError:
        error('Invalid json file %s' % click.format_filename(os.path.abspath(file_path)))

    def check_key(key):
        try:
            content[key]
        except KeyError:
            error('Invalid json structure: should have "%s" key at root' % key)

    for k in ('accesses', 'accounts', 'operations', 'settings'):
        check_key(k)

    return content


def write_json_file(output_path, content):
    o_path = os.path.abspath(output_path)
    try:
        with open(o_path, 'w') as output_file:
            output_file.write(json.dumps(content))
            output_file.close()
        click.echo("File successfully saved at %s" % click.format_filename(o_path))
    except IOError as err:
        error('Could not write to %s: %s' % (click.format_filename(o_path), err))


def select_access(accesses):
    click.echo("Select the access for which to merge tow accounts:\n")
    for i, access in enumerate(accesses):
        click.echo('%d - Bank from module "%s" with login "%s"' % (i, access['bank'], access['login']))

    selected = click.prompt("\nSelect the access", type=click.Choice([str(i) for i in range(len(accesses))]))
    return accesses[int(selected)]


def select_accounts(prompt_message, accounts):
    click.echo('\n%s\n' % prompt_message)
    for i, account in enumerate(accounts):
        click.echo('%d - %s ' % (i, format_account(account)))

    selected = click.prompt("\nSelect the account", type=click.Choice([str(i) for i in range(len(accounts))]))
    return accounts[int(selected)]


def select_candidate_access(accesses, accounts):
    selected = select_access(accesses)
    accs = [acc for acc in accounts if acc['bankAccess'] is selected['id']]

    if len(accs) < 2:
        click.echo('This access has less than 2 accounts, we cannot merge these accounts')
        click.pause()
        return select_candidate_access(accesses, accounts)
    return selected


def account_from_access_id(access_id, accounts):
    return [acc for acc in accounts if acc['bankAccess'] is access_id]


# Define the cli tool.
@click.group()
def cli():
    pass


# Define the mergeaccount command.
@cli.command(help='Merges an account into another account of the same access.')
@click.option('--input-file', help='The path to json dump of kresus db.', callback=load_json_file)
@click.option('--output-file', help='The path to the file to save the result', type=click.STRING)
def mergeaccounts(input_file, output_file):
    all_accounts = input_file['accounts']
    selected_access = select_candidate_access(input_file['accesses'], all_accounts)
    selected_accounts = account_from_access_id(selected_access['id'], all_accounts)
    to_keep = select_accounts('Choose the account %s' % click.style('TO KEEP', fg='green'), selected_accounts)
    to_delete = select_accounts('Choose the account %s' % click.style('TO DELETE', fg='green'), [acc for acc in selected_accounts if acc['id'] is not to_keep['id']])
    click.confirm('All the operations of %s will be attached to account %s. Are you sure ?' % (format_account(to_delete), format_account(to_keep)), abort=True)

    # Attach the operations from to_delete to to_keep
    click.echo('Moving operations attached to account %s' % format_account(to_delete))
    num_ops_updated = 0
    for operation in input_file['operations']:
        if operation['accountId'] == to_delete['id']:
            operation['accountId'] = to_keep['id']
            num_ops_updated += 1
    click.echo('Updated %d operations' % num_ops_updated)

    # Attach alerts to the appropriate account.
    click.echo('Moving alerts attached to account %s' % format_account(to_delete))
    num_alerts_updated = 0
    for alert in input_file['alerts']:
        if alert['accountId'] == to_delete['id']:
            alert['accountId'] = to_keep['id']
            num_alerts_updated += 1
    click.echo('Updated %d alerts' % num_alerts_updated)

    # Setting defaultAccountId if necessary.
    for setting in input_file['settings']:
        if setting['name'] != 'defaultAccountId':
            pass
        if setting['value'] == to_delete['id']:
            setting['value'] = to_keep['id']
            click.echo('Updated defaultAccountId')
        break

    input_file['accounts'].remove(to_delete)
    click.echo('Successfully deleted account %s' % format_account(to_delete))
    write_json_file(output_file, input_file)


if __name__ == '__main__':
    cli()

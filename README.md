# What does this tool do ?

This tool is meant to fix inconsistancies which could occur in Kresus, for example when the website of a bank completely changes. Kresus can be in an inconsitant state (duplicate accounts for example).

# Install
Clone this repo : `git clone https://framagit.org/ZeHiro/kresus-dump-tool` and go in the target forder.
Install the dependancies by typing `pip install --user -r ./requirements.txt`

# How to use ?

**Note this script was tested only with `python2`**

Run `./kresus_dump_tool.py --help` to get all the commands provided by the script.

The only implemented command is `mergeaccounts` it is meant to be used to attach operations and alerts from an account to another account, from the same access. This can be useful if the bank changes the format of the account number for example.
Note this script does not yet allow to deduplicate the transactions.


- Download the `kresus.json` file from you Kresus instance (Settings > Backup and restore > Export).
- Run `./kresus_dump_tool.py mergaccounts --input-file=/path/to/kresus.json --output-file=/path/to/new-kresus.json` and follow the instructions.
- Clear your Kresus (by deleting thd db for example, keep a copy just in case.) and reimport the `new-kresus.json` file.

⚠️ Always keep a backup of the original `kresus.json`. This script will not modify it, but it is always interesting to be able to comeback to the initial situation.
